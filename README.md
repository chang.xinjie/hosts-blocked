# hosts-blocked

#### 项目介绍

通过hosts文件屏蔽五大流氓和遇到的各类网站视频广告以及各网盟访问统计日志类数据

#### 使用方法

直接复制hosts文件替换掉操作系统的hosts文件(需要root/admin权限)

#### 参与贡献

欢迎补充广告列表, 采用及其严格的屏蔽策略, 加入后一般情况下不予解除屏蔽
1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 增加过滤地址后, 请务必执行排序+去重操作, Linux命令为 `sort hosts | uniq > hosts`
4. 提交代码
5. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)